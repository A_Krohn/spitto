+++
title = "Wie kann ich mich viel Bewegen?"
[taxonomies]
tags = ["move", "health", "AKrohn"]
categories = ["process"]
+++

Jeder Mensch sollte sich viel bewegen im Alltag, siehe Artikel "[Gründe für mehr Bewegung](https://spitto.eu/reason-to-move/)". 

Nachfolgend sind einige Möglichkeiten aufgelistet, wie man sich mehr bewegen kann im Alltag:

## Mehr Spazieren gehen oder mehr Fahrrad fahren
Jeder einzelne Spaziergang oder Radtour den wir Alltag mehr machen steigert unsere Fitness und unser Wohlbefinden. 10 000 Schritte oder 30 min Radfahren pro Tag sollten die magischen Ziele sein

## Parken Sie Auto möglichst weit weg 
Jedes mal wenn Sie einen Parkplatz suchen, parken Sie möglichst weit weg von den Ort entfernt, wo Sie hin möchten. Jeder Schritt den Sie nun von Ihren Parkplatz zu Ihren Ziel mehr zurücklegen ist eine Belohnung für Ihren Körper.

## Laufen Sie die Treppe und ignorieren Sie den Fahrstuhl
Jede Stufe die Sie mit Ihren ganzen Körpergewicht selber laufen bringt Ihren Kreislauf in Schwung. 

## Nutzen Sie Stehtische
Jedes mal wenn Sie einen Stehtisch nutzen bewegen Sie sich automatisch mehr und zeigen anderen Menschen das Sitzen auch stehen bedeuten kann.

## Machen Sie Ausflüge
Jeder Ausflug ist mit Bewegung verbunden und mit neuen Eindrücken, die das Leben reicher machen.

## Treiben Sie Sport
Jedes mal wenn Sie Sport treiben freut sich Ihr Körper und Ihr Geist über die Aktivität und Sie lernen andere Menschen kennen

## Laufen oder hüpfen Sie beim Fernsehen
Es sieht verrückt aus macht aber Spaß.

## Nutzen Sie öffentliche Verkehrsmittel anstatt das Auto
Sind Sie mit öffentlichen Verkehrsmitteln unterwegs, sind ein Sportler im Vergleich zu einen Autofahrer.

## Telefonieren Sie im Gehen
Jedes Telefonat sollte im Gehen erfolgen so bleiben Sie konzentrierter und tun was gutes für sich

## Abschließende Bemerkungen
Bewegung ist elementar für unseren Körper und Geist. Wir Menschen sind Ausdauersportler und brauchen Bewegung für ein langes Leben. Jeder Schritt den wir aktiv selber machen hilft. Bewegen Sie Ihr Leben!

Persönlich würde ich mich dafür interessieren, wie Sie für Bewegung im Alltag sorgen?

[Umfragelink (EXTERN)](https://www.surveymonkey.de/r/CNHRVVV) Dies ist eine Umfrage um besser zu verstehen, wie sich Menschen im Alltag bewegen. 
