+++
title="Datenschutzerklärung"
+++
## Informationen über den Verantwortlichen

Siehe (Impressum)[https://spitto.eu/impressum/]

### Besuch der Webseite

Daten über den Besucher dieser Webseite werden nicht gespeichert und ausgewertet

#### Information zu Cookies

Cookies werden nicht verwendet auf dieser Webseite

#### Information zu Analysetools

Analysetools werden nicht verwendet auf dieser Webseite

#### Information über Plug-ins

Plug-ins werden nicht verwendet auf dieser Webseite

#### Information über externe Links

Externe Links werden auf dieser Webseite verwendet. Ein externer Link wird durch die Verwendung des Schlüsselwortes "extern" gekennzeichnet. Wird ein externer Link aufgerufen, erfolgt ein Aufruf einer externen Webseite. Jede externe Webseite kann Cookies, Analysetools, Plug-Ins und ebenfalls weitere externe Links enthalten. Es gilt dann die Datenschutzerklärung dieser externen Webseite.

## Rechte der Webseitenbesucher

* Auskunftsrecht Art. 15 DSGVO
* Berichtigungsrecht Art. 16 DSGVO
* Widerspruchsrecht Art. 21 DSGVO
* Recht auf Löschung und Einschränkung der Verarbeitung von Daten Art. 17 DSGVO
* Recht auf Vergessenwerden Art. 17 DSGVO
* Recht auf Datenübertragbarkeit Art. 20
* Recht auf Beschwerde bei Aufsichtsbehörden Art. 77 DSGVO.
* Recht auf Widerruf von Einwilligungen Art. 7 Abs. 3 DSGVO.

