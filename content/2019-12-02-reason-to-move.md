+++
title = "Reason to move"
description = "Learn which reason exist for movement"
[taxonomies]
tags = ["move", "health", "AKrohn"]
categories = ["reason", "acceptance_criteria"]
+++

An active life style improve the health of people in every age. Every person even people with chronic pain can profit from the positiv effects of regular movement

Following are the main reason for movement:

## Reduced risk of cardiovascular problems
Active people have a fifty percent lower risk of cardiovasular problems than inactive people. Also active people have a lower risk of getting a stroke, getting higher blood pressure and getting a higher cholesterol level

## Reduced risk of overweight and obesity
Active people have a reduced risk of getting overweight or obesity. Active people can hold a body weight which is healthy over the whole life time.
Inactive people can reduce their overweight with movement

## Reduced risk of diabetes
Active people have a thirty percent lower risk of getting type 2 diabetes than inactive people. This happen when a moderate or high intensity movement is done regularity.

## Reduced risk of cancer
Active people have a forty percent lower risk of getting colon cancer. Further active people have a lower risk of getting breast cancer or prostate cancer.

## Reduced risk of musculoskeletal disorders
Active people who do movement over the whole life time have lower risk of musculoskeletal disorders in high age. Further active people have a reduced risk of spills and hip fractures.

## Reduced risk of depression and stress
Active people have a lower risk of getting a depression or stress. Active children have a higher social competence. Active people have a higher self-confidence.

## Positive impact on social life in communities
Active people have a higher chance of meeting new people. The crime rate is lower when more people are active. The parks and green areas are cleaner in communities with high percent of active people.

## Positive impact on economic
Active people reduce the cost for the healthcare system. One inactive person cost the goverment up to 300 euro per year.

## Positive impact for health in general
Active people eat healthier and smoke less.

## Final remarks
These all are important reason for more movement. This is especially important because the number of inactive people have been increased. For example currently more the 42 percent of the people in germany do move less than 150 minutes per week or do less than 75 minutes sport.

Can you give me you answer why you personally want to move more?

[Extern survey link](https://www.surveymonkey.de/r/GZFJ6L3)
This is a survey to get a better understanding why people like to move.
